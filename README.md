## INTRODUCTION
Telegram login allows users to register and login to your Drupal site with their telegram account.
The module uses official telegram [login widget][2]

## REQUIREMENTS
[Social Auth module][1]

## CONFIGURATION
1. You need to register a new telegram bot. Visit @BotFather and enter command `/newbot` and follow bot instructions. Detailed information about telegram bots is [here][3]
2. When the bot is created you need to add allowed domains to this bot. Visit @BotFather and enter command `/setdomain`. It offers you to select a bot and enter domain of your site, as example https://drupal.org.
3. Go to a telegram login settings and enter bot access token which you received when it was created. Configuration->Social API settings->User authentication->Telegram tab (admin/config/social-api/social-auth/telegram)
4. Render a login link where you need. To render a login link use social_auth_telegram_link theme:
```php
  return [
    '#theme' => 'social_auth_telegram_link',
    '#info' => [
      'title' => t('Login via telegram'),
    ],
  ];
```

[1]: https://www.drupal.org/project/social_auth
[2]: https://core.telegram.org/widgets/login
[3]: https://core.telegram.org/bots#3-how-do-i-create-a-bot
