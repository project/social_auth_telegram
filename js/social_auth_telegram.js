(function ($, Drupal) {
  Drupal.behaviors.social_auth_telegram = {
    attach: function (context, settings) {
      $('.js-social-auth-telegram-link').once('social_auth_telegram').each(function(){
        $(this).on('click', function(){
          var bot_id = $(this).attr('data-bot_id');
          socialAuthTelegramLogin(bot_id);
        });
      });
    }
  }

  window.socialAuthTelegramLogin = function(bot_id) {
    var s = document.createElement('script');
    s.setAttribute( 'src', 'https://telegram.org/js/telegram-widget.js');
    s.onload = window.Telegram.Login.auth({
        bot_id: bot_id,
        request_access: false
      },
      function(data) {
        Drupal.ajax({
          url: '/user/login/telegram/callback',
          submit: { 'telegram_data': data },
          progress: { type: 'none' }
        }).execute();
      }
    );
    document.head.appendChild(s);
  }

})(jQuery, Drupal);
