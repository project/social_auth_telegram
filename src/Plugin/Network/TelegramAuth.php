<?php

namespace Drupal\social_auth_telegram\Plugin\Network;

use Drupal\Core\Url;
use Drupal\social_api\SocialApiException;
use Drupal\social_auth\Plugin\Network\NetworkBase;

/**
 * Defines a Network Plugin for Social Auth Telegram.
 *
 * @package Drupal\social_auth_telegram\Plugin\Network
 *
 * @Network(
 *   id = "social_auth_telegram",
 *   social_network = "Telegram",
 *   type = "social_auth",
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_telegram\Settings\TelegramAuthSettings",
 *       "config_id": "social_auth_telegram.settings"
 *     }
 *   }
 * )
 */
class TelegramAuth extends NetworkBase {

  /**
   * {@inheritdoc }
   */
  protected function initSdk() {
    return FALSE;
  }

}
