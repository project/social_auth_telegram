<?php

namespace Drupal\social_auth_telegram\Settings;

use Drupal\social_api\Settings\SettingsBase;

/**
 * Defines methods to get Social Auth Telegram settings.
 */
class TelegramAuthSettings extends SettingsBase {

  /**
   * Bot token.
   *
   * @var string
   */
  protected $token;

  /**
   * {@inheritdoc}
   */
  public function getToken() {
    if (!$this->token) {
      $this->token = $this->config->get('bot_token');
    }
    return $this->token;
  }

}
