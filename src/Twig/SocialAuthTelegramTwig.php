<?php

namespace Drupal\social_auth_telegram\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Add function to render a telegram link in a twig template.
 */
class SocialAuthTelegramTwig extends AbstractExtension {

  /**
   * Returns the telegram twig extension name.
   */
  public function getName() {
    return 'social_auth_telegram_twig_extension';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('social_auth_telegram_link', [self::class, 'link']),
    ];
  }

  /**
   * Return a telegram login link.
   *
   * @param $title
   *   Button title
   *
   * @return array
   *   Render array with the telegram login link.
   */
  public static function link($title = NULL): array {
    return [
      '#theme' => 'social_auth_telegram_link',
      '#info'  => [
        'title' => $title
      ],
    ];
  }

}
