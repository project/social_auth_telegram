<?php

namespace Drupal\social_auth_telegram\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\social_auth\Form\SocialAuthSettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TelegramAuthSettingsForm extends SocialAuthSettingsForm {

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_auth_telegram_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return array_merge(
      parent::getEditableConfigNames(),
      ['social_auth_telegram.settings']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_auth_telegram.settings');
    $form['telegram_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Telegram settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#description' => $this->t('You need to first create a Telegram bot and setup your domain. <a href="@telegram-bot">Instruction</a>', ['@telegram-bot' => 'https://core.telegram.org/widgets/login']),
    ];

    $form['telegram_settings']['bot_token'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Bot token'),
      '#default_value' => $config->get('bot_token'),
    ];

    $options = [];
    $fields = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    foreach ($fields as $key => $field) {
      if ($field instanceof FieldConfig) {
        $options[$key] = $field->getLabel() . '(' . $key . ')';
      }
    }

    $telegram_fields = [
      'first_name' => $this->t('First name'),
      'last_name'  => $this->t('Last name'),
      'username'   => $this->t('Telegram name'),
    ];

    $form['telegram_settings']['field_map'] = [
      '#type'  => 'details',
      '#title' => $this->t('Field map'),
    ];
    foreach ($telegram_fields as $key => $telegram_field) {
      $form['telegram_settings']['field_map'][$key] = [
        '#type'          => 'select',
        '#title'         => $telegram_field,
        '#options'       => $options,
        '#empty_option'  => $this->t('Select'),
        '#default_value' => $config->get('field_map.' . $key),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('telegram_settings');
    $config = $this->config('social_auth_telegram.settings');
    foreach ($values as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
