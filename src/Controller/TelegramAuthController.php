<?php

namespace Drupal\social_auth_telegram\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\Controller\OAuth2ControllerBase;
use Drupal\social_auth\SocialAuthDataHandler;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_telegram\TelegramAuthManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Social Auth Telegram routes.
 */
class TelegramAuthController extends OAuth2ControllerBase {

  public function __construct(MessengerInterface $messenger,
                              NetworkManager $network_manager,
                              UserAuthenticator $user_authenticator,
                              TelegramAuthManager $telegram_manager,
                              RequestStack $request,
                              SocialAuthDataHandler $data_handler,
                              RendererInterface $renderer) {

    parent::__construct('Social Auth Telegram', 'social_auth_telegram',
      $messenger, $network_manager, $user_authenticator,
      $telegram_manager, $request, $data_handler, $renderer);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('messenger'),
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_authenticator'),
      $container->get('social_auth_telegram.manager'),
      $container->get('request_stack'),
      $container->get('social_auth.data_handler'),
      $container->get('renderer')
    );
  }

  /**
   * Response for path 'user/login/telegram/callback'.
   *
   * Telegram returns user data after authentication.
   */
  public function callback(): AjaxResponse {
    $data = \Drupal::request()->request->get('telegram_data');
    $response = new AjaxResponse();
    try {
      if (!is_array($data)) {
        throw new \Exception('Empty data');
      }
      $data = $this->verify($data);
      $authenticatorResponse = $this->userAuthenticator->authenticateUser('telegram_' . $data['id'],
        NULL,
        $data['id'],
        NULL,
        $data['photo_url'] ?? NULL,
        $data
      );
      if ($authenticatorResponse instanceof RedirectResponse) {
        $url = $authenticatorResponse->getTargetUrl();
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
    }
    if (empty($url)) {
      $url = Url::fromRoute('user.login')->toString();
    }
    $response->addCommand(new RedirectCommand($url));
    return $response;
  }

  /**
   * Verifies telegram data.
   *
   * @param $auth_data
   *   Telegram authenticate data.
   *
   * @returns array
   *   Array with the telegram data.
   * @throws \Exception
   */
  function verify(array $auth_data): array {
    $token = $this->config('social_auth_telegram.settings')->get('bot_token');
    $check_hash = $auth_data['hash'];
    unset($auth_data['hash']);
    $data_check_arr = [];
    foreach ($auth_data as $key => $value) {
      $data_check_arr[] = $key . '=' . $value;
    }
    sort($data_check_arr);
    $data_check_string = implode("\n", $data_check_arr);
    $secret_key = hash('sha256', $token, true);
    $hash = hash_hmac('sha256', $data_check_string, $secret_key);
    if (strcmp($hash, $check_hash) !== 0) {
      throw new \Exception('Data is NOT from Telegram');
    }
    if ((time() - $auth_data['auth_date']) > 86400) {
      throw new \Exception('Data is outdated');
    }
    return $auth_data;
  }

}
