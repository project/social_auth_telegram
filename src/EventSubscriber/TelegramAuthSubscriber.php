<?php

namespace Drupal\social_auth_telegram\EventSubscriber;

use Drupal\social_auth\Event\SocialAuthEvents;
use Drupal\social_auth\Event\UserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Reacts on Social Auth events.
 */
class TelegramAuthSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[SocialAuthEvents::USER_CREATED] = ['onUserCreated'];
    return $events;
  }

  /**
   * Fills a user fields by telegram data fields.
   */
  public function onUserCreated(UserEvent $event) {
    try {
      $socialAuthUser = $event->getSocialAuthUser();
      $user = $event->getUser();
      $fields = \Drupal::config('social_auth_telegram.settings')->get('field_map');
      $data = $socialAuthUser->getAdditionalData();
      foreach ($fields as $telegram_field => $user_field) {
        if (!empty($data[$telegram_field]) && $user->hasField($user_field)) {
          $user->set($user_field, $data[$telegram_field]);
        }
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('social_auth_telegram')->error($e->getMessage());
    }
  }

}
